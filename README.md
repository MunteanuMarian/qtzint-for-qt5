QtLibZint and QtZint for Qt5


Open QtLibZint.pro and set LIBPNG_DIR and ZLIB_DIR with LibPNG and Zlib header files
Ex.:
LIBPNG_DIR = D:\INCLUDE\libpng
ZLIB_DIR = D:\INCLUDE\zlib

Open QtZint.pro and set EXTRALIBS_DIR with the directory where you have libpng.lib and zlib.lib
Ex.:
EXTRALIBS_DIR = D:\LIB\


Open Visual Studio Command Prompt, add QMAKE to PATH (Ex. set PATH=%PATH%;D:\Qt\bin ),
navigate to QtZint main folder, and execute build.cmd .
