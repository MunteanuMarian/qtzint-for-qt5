EXTRALIBS_DIR = 

QT += gui widgets uitools

TEMPLATE = app
TARGET = QtZint
INCLUDEPATH += QtLibZint QtZint
LIBS += -L../QtLibZint -L$$EXTRALIBS_DIR -lQtZint2 -llibpng -lzlib

HEADERS += \
QtZint/barcodeitem.h \
QtZint/mainwindow.h \
QtZint/datawindow.h \
QtZint/exportwindow.h \
QtZint/sequencewindow.h

FORMS += QtZint/grpAztec.ui \
QtZint/extData.ui \
QtZint/extExport.ui \
QtZint/extSequence.ui \
         QtZint/grpC128.ui \
         QtZint/grpC16k.ui \
         QtZint/grpC39.ui \
         QtZint/grpC49.ui \
         QtZint/grpChannel.ui \
         #grpCodablock.ui \
         QtZint/grpCodeOne.ui \
         QtZint/grpDM.ui \
         QtZint/grpMaxicode.ui \
         QtZint/grpMicroPDF.ui \
         QtZint/grpMQR.ui \
         QtZint/grpMSICheck.ui \
         QtZint/grpPDF417.ui \
         QtZint/grpQR.ui \
         QtZint/mainWindow.ui

SOURCES += \
QtZint/barcodeitem.cpp \
QtZint/main.cpp \
QtZint/mainwindow.cpp \
QtZint/datawindow.cpp \
QtZint/sequencewindow.cpp \
QtZint/exportwindow.cpp

RESOURCES += QtZint/resources.qrc
