LIBPNG_DIR = 
ZLIB_DIR = 

win32 {
	TEMPLATE = lib
	CONFIG += staticlib release
}

TARGET = QtZint2
VERSION = 2.4.3

QMAKE_CFLAGS += /TP /wd4018 /wd4244 /wd4305
QMAKE_CXXFLAGS += /TP /wd4018 /wd4244 /wd4305

INCLUDEPATH += \
$$LIBPNG_DIR \
$$ZLIB_DIR

DEFINES +=  _CRT_SECURE_NO_WARNINGS _CRT_NONSTDC_NO_WARNINGS ZINT_VERSION=\"$$VERSION\"

!contains(DEFINES, NO_PNG) {
    SOURCES += QtLibZint/png.c
}

HEADERS +=  QtLibZint/aztec.h \
            QtLibZint/code1.h \
            QtLibZint/code49.h \
            QtLibZint/common.h \
            QtLibZint/composite.h \
            QtLibZint/dmatrix.h \
            QtLibZint/font.h \
            QtLibZint/gb2312.h \
            QtLibZint/gridmtx.h \
            QtLibZint/gs1.h \
            QtLibZint/large.h \
            QtLibZint/maxicode.h \
            QtLibZint/maxipng.h \
            QtLibZint/ms_stdint.h \
            QtLibZint/pdf417.h \
            QtLibZint/qr.h \
            QtLibZint/reedsol.h \
            QtLibZint/rss.h \
            QtLibZint/sjis.h \
            QtLibZint/zint.h \
            QtLibZint/qzint.h

SOURCES += QtLibZint/2of5.c \
           QtLibZint/auspost.c \
           QtLibZint/aztec.c \
           QtLibZint/code.c \
           QtLibZint/code1.c \
           QtLibZint/code128.c \
           QtLibZint/code16k.c \
           QtLibZint/code49.c \
           QtLibZint/common.c \
           QtLibZint/composite.c \
           QtLibZint/dmatrix.c \
           QtLibZint/gridmtx.c \
           QtLibZint/gs1.c \
           QtLibZint/imail.c \
           QtLibZint/large.c \
           QtLibZint/library.c \
           QtLibZint/maxicode.c \
           QtLibZint/medical.c \
           QtLibZint/pdf417.c \
           QtLibZint/plessey.c \
           QtLibZint/postal.c \
           QtLibZint/ps.c \
           QtLibZint/qr.c \
           QtLibZint/reedsol.c \
           QtLibZint/render.c \
           QtLibZint/rss.c \
           QtLibZint/svg.c \
           QtLibZint/telepen.c \
           QtLibZint/upcean.c \
           QtLibZint/qzint.cpp

